package org.bitbucket.infovillafoundation.denkolocation;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DaoGen {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "org.bitbucket.infovillafoundation.denkolocationfinder.dao");

        Entity denkoStation = schema.addEntity("DenkoStation");
        denkoStation.addIdProperty();
        denkoStation.addStringProperty("stationNameEnglish");
        denkoStation.addStringProperty("stationNameMyanmar");
        denkoStation.addDoubleProperty("latitude");
        denkoStation.addDoubleProperty("longitude");
        denkoStation.addStringProperty("stationAddressEnglish");
        denkoStation.addStringProperty("stationAddressMyanmar");

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }
}
