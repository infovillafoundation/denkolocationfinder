package org.bitbucket.infovillafoundation.denkolocationfinder.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.bitbucket.infovillafoundation.denkolocationfinder.R;
import org.bitbucket.infovillafoundation.denkolocationfinder.adapter.DenkoStationListAdapter;
import org.bitbucket.infovillafoundation.denkolocationfinder.application.DenkoLocationFinderApplication;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DaoMaster;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DaoSession;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DenkoStationDao;
import org.bitbucket.infovillafoundation.denkolocationfinder.geo.DenkoLocationListener;
import org.bitbucket.infovillafoundation.denkolocationfinder.model.DenkoStationReport;
import org.bitbucket.infovillafoundation.denkolocationfinder.rest.DefaultDenkoStationRestService;
import org.bitbucket.infovillafoundation.denkolocationfinder.rest.DenkoStationRestService;

import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LocationFinderActivity extends Activity {

    TextView latitudeTextView;

    TextView longitudeTextView;

    TextView stationNameTextView;

    TextView stationNameMyanmarTextView;

    DenkoStation selectedStation;

    ListView denkoStationListView;

    Button setCoordinateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_finder);

        latitudeTextView = (TextView) findViewById(R.id.latitude_text);
        longitudeTextView = (TextView) findViewById(R.id.longitude_text);
        stationNameTextView = (TextView) findViewById(R.id.station_name_en);
        stationNameMyanmarTextView = (TextView) findViewById(R.id.station_name_mm);
        denkoStationListView = (ListView) findViewById(R.id.denko_station_list);

        latitudeTextView.setText(DenkoLocationListener.latitude + "");
        longitudeTextView.setText(DenkoLocationListener.longitude + "");

        DaoMaster daoMaster = new DaoMaster(DenkoLocationFinderApplication.getDatabase());
        DaoSession daoSession = daoMaster.newSession();
        DenkoStationDao denkoStationDao = daoSession.getDenkoStationDao();

        List<DenkoStation> denkoStationList = denkoStationDao.loadAll();
        final DenkoStationListAdapter adapter = new DenkoStationListAdapter(this, denkoStationList);
        denkoStationListView.setAdapter(adapter);

        denkoStationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DenkoStation denkoStation = adapter.getItem(position);
                selectedStation = denkoStation;
                stationNameTextView.setText(denkoStation.getStationNameEnglish());
                stationNameMyanmarTextView.setText(denkoStation.getStationNameMyanmar());
            }
        });

        final Callback<List<DenkoStationReport>> callback = new Callback<List<DenkoStationReport>>() {
            @Override
            public void success(List<DenkoStationReport> denkoStationReport, Response response) {
                toast(R.string.success);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("fail", "fail: " + error.toString());
                toast(R.string.fail);
            }

            public void toast(int textId) {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_layout,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.toast_text);
                text.setText(getResources().getString(textId));

                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
            }
        };

        setCoordinateButton = (Button) findViewById(R.id.set_coordinate_button);

        setCoordinateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedStation != null) {
                    DenkoStationReport report = new DenkoStationReport();
                    report.setLatitude(DenkoLocationListener.latitude);
                    report.setLongitude(DenkoLocationListener.longitude);
                    report.setReportDate(new Date());
                    report.setReportId("none");
                    report.setStationName(selectedStation.getStationNameEnglish());

                    DenkoStationRestService service = new DefaultDenkoStationRestService();
                    service.fetchDenkoReport(report, callback);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DenkoLocationFinderApplication.getDatabase().close();
    }
}
