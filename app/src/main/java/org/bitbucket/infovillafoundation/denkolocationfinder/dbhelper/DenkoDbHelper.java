package org.bitbucket.infovillafoundation.denkolocationfinder.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DaoMaster;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DaoSession;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DenkoStationDao;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandah Aung on 27/3/15.
 */

public class DenkoDbHelper extends DaoMaster.DevOpenHelper {

    private Context context;

    public DenkoDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);

        String jsonString = readJsonInString("denkostationlist.json", context);

        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        DenkoStationDao denkoStationDao = daoSession.getDenkoStationDao();
        List<DenkoStation> denkoStations = convertData(jsonString);
        denkoStationDao.insertInTx(denkoStations);
    }

    private List<DenkoStation> convertData(String jsonString) {

        ArrayList<DenkoStation> denkoStations = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                DenkoStation denkoStation = new DenkoStation();

                denkoStation.setId(jsonObject.getLong("id"));
                denkoStation.setStationNameEnglish(jsonObject.getString("stationNameEnglish"));
                denkoStation.setStationNameMyanmar(jsonObject.getString("stationNameMyanmar"));
                denkoStation.setLatitude(jsonObject.getDouble("latitude"));
                denkoStation.setLongitude(jsonObject.getDouble("longitude"));
                denkoStation.setStationAddressEnglish(jsonObject.getString("stationAddressEnglish"));
                denkoStation.setStationAddressMyanmar(jsonObject.getString("stationAddressMyanmar"));

                denkoStations.add(denkoStation);
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return denkoStations;
    }

    private String readJsonInString(String fileName, Context c) {
        try {
            InputStream is = c.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer);
            return text;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
