package org.bitbucket.infovillafoundation.denkolocationfinder.rest;

import org.bitbucket.infovillafoundation.denkolocationfinder.model.DenkoStationReport;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.PUT;

/**
 * Created by Sandah Aung on 29/3/15.
 */

public interface DenkoStationRestService {

    @POST("/denkostationreport/")
    void fetchDenkoReport(@Body DenkoStationReport denkoStationReport, Callback<List<DenkoStationReport>> callback);

}
