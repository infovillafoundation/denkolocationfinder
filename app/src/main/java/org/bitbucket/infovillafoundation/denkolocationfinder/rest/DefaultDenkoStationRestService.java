package org.bitbucket.infovillafoundation.denkolocationfinder.rest;

import org.bitbucket.infovillafoundation.denkolocationfinder.converter.JacksonConverter;
import org.bitbucket.infovillafoundation.denkolocationfinder.model.DenkoStationReport;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.Body;

/**
 * Created by Sandah Aung on 29/3/15.
 */

public class DefaultDenkoStationRestService implements DenkoStationRestService {

    private DenkoStationRestService denkoStationRestService;
    private JacksonConverter jacksonConverter;

    @Override
    public void fetchDenkoReport(@Body DenkoStationReport denkoStationReport, Callback<List<DenkoStationReport>> callback) {
        if (jacksonConverter == null)
            jacksonConverter = new JacksonConverter();
        if (denkoStationRestService == null) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://denko.jelastic.skali.net/denkoservice/")
                    .setConverter(jacksonConverter)
                    .build();

            denkoStationRestService = restAdapter.create(DenkoStationRestService.class);
        }

        denkoStationRestService.fetchDenkoReport(denkoStationReport, callback);
    }
}
