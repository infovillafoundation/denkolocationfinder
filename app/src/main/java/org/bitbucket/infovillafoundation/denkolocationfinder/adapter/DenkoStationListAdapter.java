package org.bitbucket.infovillafoundation.denkolocationfinder.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.denkolocationfinder.R;
import org.bitbucket.infovillafoundation.denkolocationfinder.dao.DenkoStation;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Sandah Aung on 12/4/15.
 */

public class DenkoStationListAdapter extends ArrayAdapter<DenkoStation> {

    private final Context context;
    private final List<DenkoStation> values;

    public DenkoStationListAdapter(Context context, List<DenkoStation> values) {
        super(context, R.layout.activity_location_finder, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.denko_station_list_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        if (readLanguage().equals("en")) {
            holder.stationAddress.setText(values.get(position).getStationAddressEnglish());
        } else {
            holder.stationAddress.setText(values.get(position).getStationAddressMyanmar());
        }

        holder.stationName.setText(values.get(position).getStationNameEnglish());

        return view;
    }

    static class ViewHolder {
        @InjectView(R.id.denko_station_name_in_list)
        TextView stationName;
        @InjectView(R.id.denko_station_address_in_list)
        TextView stationAddress;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    private SharedPreferences obtainSharedPreferences() {
        return context.getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE);
    }

    private String readLanguage() {
        return obtainSharedPreferences().getString("org.bitbucket.infovillafoundation.denko.language", "en");
    }
}
