package org.bitbucket.infovillafoundation.denkolocationfinder.dao;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 *
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig denkoStationDaoConfig;

    private final DenkoStationDao denkoStationDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        denkoStationDaoConfig = daoConfigMap.get(DenkoStationDao.class).clone();
        denkoStationDaoConfig.initIdentityScope(type);

        denkoStationDao = new DenkoStationDao(denkoStationDaoConfig, this);

        registerDao(DenkoStation.class, denkoStationDao);
    }

    public void clear() {
        denkoStationDaoConfig.getIdentityScope().clear();
    }

    public DenkoStationDao getDenkoStationDao() {
        return denkoStationDao;
    }

}
