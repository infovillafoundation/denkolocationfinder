package org.bitbucket.infovillafoundation.denkolocationfinder.customui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Sandah Aung on 7/4/15.
 */


public class MyanmarOnlyTextView extends TextView {

    public MyanmarOnlyTextView(Context context) {
        super(context);
        String fontName = "fonts/WINNWAB.ttf";
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(face);
    }

    public MyanmarOnlyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        String fontName = "fonts/WINNWAB.ttf";
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(face);
    }

    public MyanmarOnlyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        String fontName = "fonts/WINNWAB.ttf";
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(face);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}
