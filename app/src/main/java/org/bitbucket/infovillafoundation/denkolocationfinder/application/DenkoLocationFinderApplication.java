package org.bitbucket.infovillafoundation.denkolocationfinder.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.DisplayMetrics;

import org.bitbucket.infovillafoundation.denkolocationfinder.dbhelper.DenkoDbHelper;
import org.bitbucket.infovillafoundation.denkolocationfinder.geo.DenkoLocationListener;

import java.util.Locale;

/**
 * Created by Sandah Aung on 26/3/15.
 */

public class DenkoLocationFinderApplication extends Application {

    private static Context initialContext;
    private static SQLiteDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        initialContext = getApplicationContext();
        setupDb();
        setupLanguage();
        registerLocationListener();
    }

    private void setupDb() {
        DenkoDbHelper helper = new DenkoDbHelper(initialContext, "denko-stations-db", null);
        database = helper.getWritableDatabase();
    }

    public static Context getAppContext() {
        return initialContext;
    }

    public static SQLiteDatabase getDatabase() {
        return database;
    }

    private void setupLanguage() {
        setLocale(readLanguage());
    }

    private void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    private void registerLocationListener() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener mlocListener = new DenkoLocationListener();
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 0, mlocListener);
        DenkoLocationListener.isGpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private SharedPreferences obtainSharedPreferences() {
        return getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE);
    }

    private String readLanguage() {
        return obtainSharedPreferences().getString("org.bitbucket.infovillafoundation.denko.language", "en");
    }
}
